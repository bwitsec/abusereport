#!/usr/bin/env bash
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

_usage="usage: $(basename "$0") <path/to/dfn-cert-mails>"
usage() {
    echo "Parse DFN-CERT emails and extract data"
    echo "${_usage}"
}

# check directory
[ -z "$1" ] && { usage; exit 1; }
indir="${1:-./}"
[ ! -d "${indir}" ] && { echo "Error: ${indir} not a directory"; usage; exit 1; }
indir="$(realpath "${indir}")"
#pdir="${indir}/dfn-credentials"

cd "${indir}" || exit 1
#mkdir -vp "${pdir}" 1>&2

# parse files and output credential list
# format: date,tag,subject-message,ip 
find "${indir}" -maxdepth 1 -type f -iname 'DFN-CERT*.eml' | while read -r mail; do 
    echo "Info: parse ${mail}" 1>&2
    date=$(grep -i "^date:" "${mail}" | cut -d":" -f2 | xargs -I {} date -d {} +%Y-%m-%d)
    tag=$(grep -i "^subject:" "${mail}" | grep -Esho "[[:digit:]]{4}-[[:digit:]]{10}") #cut -d":" -f2 | sed 's/.*#\([[:digit:]]{4}\-[[:digit:]]{10}\).*/\1/') 
    message=$(grep -i "^subject:" "${mail}" | cut -d"/" -f3 | xargs | tr ',' ' ') #cut -d":" -f2 | sed 's/.*#\([[:digit:]]{4}\-[[:digit:]]{10}\).*/\1/') 
    grep -Esh "^[[:digit:]]{3}\|" "$mail" | while IFS=$'\n' read -r line; do
        readarray -d "|" -t data <<< "$line" 
        printf "%s,%s,%s,%s\n" "$date" "${data[1]//[$'\t\r\n']}" "$tag" "${message//[$'\t\r\n']}" 
    done
    #mv -v "${mail}" "${pdir}" 1>&2
done
