#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained
# with the works, so that any entity that uses the works is notified of this
# instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# import-abuses.py
# parse and evaluate abuse reports from several sources.
# supports shadowserver (.csv) and dfn-cert (.xml) reports.
# this script takes the report files, parses them, # eliminates duplicates,
# and generates a per host summary.
# It can serialize results to a sqlite3 database and to .csv files.
###

import os
import sys
import glob
from lib.shadowserver import read_shadowserver
from lib.dfncert import read_dfncert, write_dfnxml
from lib.table import NetworkSchema, IPaddress, Table, table2csv, table2sql, write_csv, executesql

from datetime import datetime

from pytz import timezone
import getopt
import sqlite3
try:
    import dateutil.parser as dateparse
except (ImportError, ModuleNotFoundError) as e:
    print (e)
    import pip
    pip.main(['install', '--user', 'python-dateutil'])
    import dateutil.parser as dateparse


class AbuseRecord(object):
    __schema = NetworkSchema({ 'timestamp': (0, datetime), 'ip': (1, IPaddress), 'tag': (2, str), 'count': (3, int), 'raw': (4, str)})
    totimestamp = lambda x: dateparse.isoparse(x).astimezone(timezone("UTC"))

    @staticmethod
    def schema():
        return AbuseRecord.__schema

    def __init__(self, timestamp: datetime, ip: IPaddress, tag: str, count: int = 1, raw: str = ""):
        self.__timestamp = AbuseRecord.totimestamp(timestamp)
        #self.__ip = ipaddress.IPv4Address(ip)
        self.__ip = IPaddress(ip)
        self.__tag = tag
        self.__count = count
        self.raw = raw

    timestamp = property(lambda self: self.__timestamp.isoformat(), lambda self, ts: setattr(self, '_AbuseRecord__timestamp', AbuseRecord.totimestamp(ts)))
    #ip = property(lambda self: str(self.__ip), lambda self, ip: setattr(self, '_AbuseRecord__ip', ipaddress.IPv4Address(ip)))
    ip = property(lambda self: str(self.__ip), lambda self, ip: setattr(self, '_AbuseRecord__ip', IPaddress(ip)))
    tag = property(lambda self: self.__tag)
    count = property(lambda self: self.__count)

    def update(self, timestamp):
        self.__timestamp = AbuseRecord.totimestamp(timestamp)
        self.__count += 1

    def __str__(self):
        return str(tuple(map(str, [self.timestamp, self.ip, self.tag, self.count, self.raw])))

    def __repr__(self):
        return "AbuseRecord(" + self.timestamp + self.ip + ", " + self.tag + ", " + self.count + repr(self.raw) + ")"

    def astuple(self):
        return tuple(map(str, [self.timestamp, self.ip, self.tag, self.count, self.raw]))


# parses all report files and returns a list of tables with raw data
def parse_reports(optargs):
    reports = []
    if optargs.shadowserver:
        reports.extend(read_shadowserver(optargs.shadowserver))
    if optargs.dfncert:
        reports.extend(read_dfncert(optargs.dfncert))
    return reports


class ListHosts(list):
    def __init__ (self):
        super().__init__(self)

    def append(self, ts, ip, tag, raw):
        list.append(self, AbuseRecord(ts, ip, tag, 1, raw))

class DictHosts(dict):
    def __init__ (self):
        super().__init__(self)

    def append(self, ts, ip, tag, raw):
        try:
            self[(ip, tag)].update(ts)
        except KeyError:
            self[(ip, tag)] = AbuseRecord(ts, ip, tag, 1, raw)

    def __iter__(self):
        return self.values().__iter__()


# checks all records of all tables and build a new host table with aggregated host information
def build_abuse_table(tables: list) -> Table:
    hosts = DictHosts()
    #hosts = ListHosts()
    for t in tables:
        ts_idx = t.schema.indexof('timestamp')
        ip_idx = t.schema.indexof('ip') or t.schema.indexof('src_ip')
        tag = t.name
        for trecord in t:
            try:
                ts = trecord[ts_idx]
                ip = trecord[ip_idx]
            except TypeError as e:
                print("Error: %s on table %s and record %s" %(e, t.name, trecord), file=sys.stderr)
                print("Indexes: [ts] %s, [ip] %s" %(ts_idx, ip_idx), file=sys.stderr)
                print("Schema: %s" %t.schema, file=sys.stderr)
                sys.exit(1)
            rawkeys = [k for k in t.schema.keys() if k not in AbuseRecord.schema().keys()]
            raw = ""
            for k in rawkeys:
                try:
                    raw += str(k) + "=" + str(trecord[t.schema[k][0]]).replace(',', ';') + "|"
                except IndexError as e:
                    print("Warn: missing key '%s' in table '%s' ('%s','%s'), ignoring" %(k, t.name, trecord[ts_idx], trecord[ip_idx]), file=sys.stderr)
                    #raise IndexError("%s at key '%s' in record %s" %(e,k,trecord))
            raw = raw[:-1]
            hosts.append(ts, ip, tag, raw)
    tablename = "abuses"
    #schema = NetworkSchema({ 'timestamp': (0, datetime), 'ip': (1, ipaddress.IPv4Address), 'tag': (2, str), 'count': (3, int), 'raw': (4, str)})
    schema = NetworkSchema({ 'timestamp': (0, datetime), 'ip': (1, IPaddress), 'tag': (2, str), 'count': (3, int), 'raw': (4, str)})
    schema.primarykey = set(('ip', 'tag'))
    table = Table(tablename, schema)
    for host in hosts:
        table.add(host.astuple())
    return table


def usage():
    print (sys.argv[0] + " <-s|-c indir|infile> [-d database] [-o outdir] [-r]")
    print ("Parse and evaluate shadowserver report files.")
    print ("\t-s|--shadowserver: path shadowserver report(s) (.csv)")
    print ("\t-c|--dfncert: path to dfn-cert report(s) (.xml)")
    print ("\t-d|--db: path to sqlite3 database")
    print ("\t-o|--outdir: directory for output files. No output if omitted")
    #print ("\t-r|--remove: remove input files after processing (UNIMPLEMENTED)")
    sys.exit(2)


def optarg(argv):
    _shadowserver = None
    _dfncert = None
    _db = None
    _outdir = None
    _rm = False

    try:
        opts, args = getopt.getopt(argv, "hs:c:d:o:r", ["help", "shadowserver=", "dfncert=", "db=", "outdir=", "remove"])
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                usage()
            elif opt in ("-s", "--shadowserver"):
                if os.path.isdir(arg):
                    _shadowserver = glob.glob(os.path.join(arg, "")+"*.csv")
                elif os.path.isfile(arg):
                    _shadowserver = [ arg ]
                else:
                    print("ERROR: '%s' not found" % arg, file=sys.stderr)
                    usage()
            elif opt in ("-c", "--dfncert"):
                if os.path.isdir(arg):
                    _dfncert = glob.glob(os.path.join(arg, "")+"*.xml")
                elif os.path.isfile(arg):
                    _dfncert = [arg]
                else:
                    print("ERROR: '%s' not found" % arg, file=sys.stderr)
                    usage()
            elif opt in ("-d", "--db"):
                if not os.path.isfile(arg):
                    print ("WARN: '%s' not found, creating new" % arg, file=sys.stderr)
                _db = arg
            elif opt in ("-o", "--outdir"):
                if not os.path.isdir(arg):
                    print ("ERROR: '%s' not found" % arg, file=sys.stderr)
                    usage()
                _outdir = arg
            elif opt in ("-r", "--remove"):
                _rm = True
    except getopt.GetoptError:
        usage()

    if _shadowserver == None and _dfncert == None:
        print ("ERROR: specify at least one of -s or -c", file=sys.stderr)
        usage()

    return type("optargs", (object,), {'shadowserver': _shadowserver, 'dfncert': _dfncert, 'db': _db, 'outdir': _outdir, 'rm': _rm})


def main(argv):
    opts = optarg(argv)
    tables = parse_reports(opts)
    abuses = build_abuse_table(tables)

    if opts.db:
        db_conn = sqlite3.connect(opts.db)
        executesql(db_conn, table2sql(abuses))
        db_conn.close()

    if opts.outdir:
        write_csv(abuses, opts.outdir)
        write_dfnxml(abuses, opts.outdir)

    return abuses


if __name__ == '__main__':
    abuses = main(sys.argv[1:])
    # print(str(tuple(abuses.schema.keys()))[1:-1])
    # for rec in abuses:
    #     print(str(rec)[1:-1])
    for line in table2csv(abuses):
        print(line)
