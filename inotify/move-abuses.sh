#!/bin/bash

INDIR="./tosort"
OUTDIR="/sorted"

[ "$#" -lt 1 ] && { echo "usage: ${0} <infile> [outdir]"; exit 1; }
[ "$#" -ge 1 ] && INDIR="${1}"
[ "$#" -ge 2 ] && OUTDIR="${2}"

mkdir -p "${OUTDIR}"
rename -v 's/-universitt_konstanz-ip//g' "${INDIR}"
mv -v "${INDIR}" "${OUTDIR}"/
