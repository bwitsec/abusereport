#!/usr/bin/env bash
command -v inotifywait >/dev/null 2>&1 || { echo >&2 "ERROR: inotifywait required (inotify-tools)."; usage; exit 1; }

BIN="./"
WATCH="./tosort"
DROP="./sorted"

mkdir -p "${WATCH}" "${DROP}"

inotifywait --monitor -r -e close_write -e moved_to --format '%e %w %f' ${WATCH} |
while read -r event watch file; do
	(
	echo "${event} in ${watch} on file ${file}"
	${BIN}/move-abuses.sh "${watch}/${file}" "${DROP}"
	)  &
done
